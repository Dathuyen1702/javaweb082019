package com.dangvandat.converter;

import com.dangvandat.dto.customerDTO;
import com.dangvandat.Entity.customerEntity;
import org.modelmapper.ModelMapper;

public class CustomerConverter {
    public customerDTO convertToDTO(customerEntity customerEntity) {
        ModelMapper modelMapper = new ModelMapper();
        @SuppressWarnings("unused")
        customerDTO result = modelMapper.map(customerEntity, customerDTO.class);
        return result;
    }

    public customerEntity convertToEntity(customerDTO customerDTO) {
        ModelMapper modelMapper = new ModelMapper();
        @SuppressWarnings("unused")
        customerEntity result = modelMapper.map(customerDTO, customerEntity.class);
        return result;
    }
}
