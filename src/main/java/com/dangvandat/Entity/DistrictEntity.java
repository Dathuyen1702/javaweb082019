package com.dangvandat.Entity;

import com.dangvandat.annotation.Column;
import com.dangvandat.annotation.Entity;

@Entity
public class DistrictEntity extends BaseEntity {
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "code")
	private String code;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
		
}
