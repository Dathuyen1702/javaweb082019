package com.dangvandat.api;

import com.dangvandat.Builder.CustomerSearchBuilder;
import com.dangvandat.paging.Pageble;
import com.dangvandat.paging.impl.PageRequest;
import com.dangvandat.dto.customerDTO;
import com.dangvandat.service.ICustomerService;
import com.dangvandat.service.iBuildingService;
import com.dangvandat.service.impl.BuildingService;
import com.dangvandat.service.impl.CustomerService;
import com.dangvandat.utils.HttpUtil;
import com.dangvandat.utils.formUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {"/api-customer"})
public class customerAPI extends HttpServlet {

    private iBuildingService buildingService;

    private ICustomerService customerService;

    public customerAPI(){
        buildingService = new BuildingService();
        customerService = new CustomerService();
    }

    protected void doGet(HttpServletRequest request , HttpServletResponse reponse)
            throws ServletException , IOException{
        ObjectMapper mapper = new ObjectMapper();
        request.setCharacterEncoding("utf-8");
        reponse.setContentType("application/json");
        customerDTO model = formUtil.toModel(customerDTO.class , request);
        CustomerSearchBuilder builder = initSearchCustomer(model);
        Pageble pageble = new PageRequest(null , null , null);
        model.setListResult(customerService.findAll(builder , pageble));
        mapper.writeValue(reponse.getOutputStream(), model.getListResult());
    }

    private CustomerSearchBuilder initSearchCustomer(customerDTO model) {
        CustomerSearchBuilder builder = new CustomerSearchBuilder.builder()
                .setFullName(model.getFullName())
                .setEmail(model.getEmail())
                .setPhone(model.getPhone())
                .setStaffId(model.getStaffId())
                .builder();
        return builder;
    }
}
