package com.dangvandat.service;

import com.dangvandat.Builder.CustomerSearchBuilder;
import com.dangvandat.dto.customerDTO;
import com.dangvandat.paging.Pageble;

import java.util.List;

public interface ICustomerService {
    List<customerDTO> findAll(CustomerSearchBuilder builder , Pageble pageble);
}
