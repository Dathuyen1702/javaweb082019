package com.dangvandat.service.impl;

import com.dangvandat.Entity.UserEntity;
import com.dangvandat.converter.UserConverter;
import com.dangvandat.dto.UserDTO;
import com.dangvandat.service.iUserService;

public class UserService implements iUserService{

	@Override
	public UserDTO save(UserDTO newUser) {
		UserConverter converter = new UserConverter();
		@SuppressWarnings("unused")
		UserEntity userEntity = converter.convertToEntity(newUser);
		
		return null;
	}
	
}
