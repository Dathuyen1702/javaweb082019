package com.dangvandat.service.impl;

import com.dangvandat.Builder.CustomerSearchBuilder;
import com.dangvandat.converter.BuildingConverter;
import com.dangvandat.converter.CustomerConverter;
import com.dangvandat.dto.customerDTO;
import com.dangvandat.Entity.customerEntity;
import com.dangvandat.paging.Pageble;
import com.dangvandat.repository.impl.CustomerRepository;
import com.dangvandat.service.ICustomerService;

import java.util.List;
import java.util.stream.Collectors;

public class CustomerService implements ICustomerService {

    private CustomerRepository customerRepository = new CustomerRepository();

    private CustomerConverter customerConverter = new CustomerConverter();

    @Override
    public List<customerDTO> findAll(CustomerSearchBuilder builder , Pageble pageble) {
        List<customerEntity> customerEntities = customerRepository.findAll(builder , pageble);
        List<customerDTO> results = customerEntities.stream().map(item -> customerConverter.convertToDTO(item)).collect(Collectors.toList());
        return results;
    }
}
