package com.dangvandat.service;

import com.dangvandat.dto.UserDTO;

public interface iUserService {
	UserDTO save(UserDTO newUser);
}
