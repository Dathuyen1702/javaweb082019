package com.dangvandat.repository;

import com.dangvandat.Builder.CustomerSearchBuilder;
import com.dangvandat.Entity.customerEntity;
import com.dangvandat.paging.Pageble;

import java.util.List;

public interface ICustomerRepository {
    List<customerEntity> findAll(CustomerSearchBuilder builder , Pageble pageble);
}
