package com.dangvandat.repository.impl;

import com.dangvandat.Builder.CustomerSearchBuilder;
import com.dangvandat.Entity.customerEntity;
import com.dangvandat.paging.Pageble;
import com.dangvandat.repository.ICustomerRepository;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomerRepository extends AbstractJDBC<customerEntity> implements ICustomerRepository {

    @Override
    public List<customerEntity> findAll(CustomerSearchBuilder builder , Pageble pageble) {
        Map<String , Object> properties = builMapSearch(builder);
        return findAll(properties , pageble);
    }

    private Map<String, Object> builMapSearch(CustomerSearchBuilder builder) {
        Map<String , Object> result = new HashMap<>();
        try{
            Field[] fields = CustomerSearchBuilder.class.getDeclaredFields();
            for (Field field : fields){
                if(!field.getName().equals("staffId")){
                    field.setAccessible(true);
                    if(field.get(builder) != null){
                        result.put(field.getName().toLowerCase() , field.get(builder));
                    }
                }
            }
        }catch (IllegalArgumentException | IllegalAccessException e){
            e.printStackTrace();
        }
        return result;
    }



   /* private Map<String, Object> builMapSearch(BuildingSearchBuilder builder) {
        Map<String , Object> result = new HashMap<>();
        try{
            Field[] fields = BuildingSearchBuilder.class.getDeclaredFields();
            for(Field field : fields){
                if(!field.getName().equals("buildingTypes")
                        && !field.getName().startsWith("costRent")
                        && !field.getName().startsWith("areaRent")){
                    field.setAccessible(true);
                    if (field.get(builder) != null){
                        if(field.getName().equals("numberOfBasement") || field.getName().equals("buildingArea")){
                            result.put(field.getName().toLowerCase() , Integer.parseInt((String) field.get(builder)));
                        }else{
                            result.put(field.getName().toLowerCase() , field.get(builder));
                        }
                    }
                }
            }
        }
        catch (IllegalArgumentException | IllegalAccessException e){
            e.printStackTrace();
        }
        return result;*/
}
